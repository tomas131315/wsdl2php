<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);
ini_set('display_errors', 1);
require_once 'vendor/autoload.php';

$wsdlFile = $argv[1];
if (!$wsdlFile || !is_file(__DIR__.'/'.$wsdlFile))
throw new \RuntimeException(sprintf('Input wsdl-file was not found'));

$outDir = __DIR__.'/out';
if (is_dir($outDir))rmdir($dirname);
if (!mkdir($outDir) && !is_dir($outDir))
throw new \RuntimeException(sprintf('Directory "%s" was not created', $outDir));

$generator = new \Wsdl2PhpGenerator\Generator();
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => $wsdlFile,
        'outputDir' => $outDir
    ))
);